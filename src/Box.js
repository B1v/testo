import React from 'react';
import './Box.css';

let items = [
    {
        color: '#414A4C',
        colorName: 'Space'
    },
    {
        color: '#C41E3A',
        colorName: 'Cardinal'
    },
    {
        color: '#4B0082',
        colorName: 'Indigo'
    },
    {
        color: '#009B77',
        colorName: 'Emerald'
    },
    {
        color: '#EFD334',
        colorName: 'Pear'
    },
    {
        color: '#F34723',
        colorName: 'Pomegranate'
    },
    {
        color: '#42AAFF',
        colorName: 'Blue'
    },
    {
        color: '#702963',
        colorName: 'Byzantium'
    },
    {
        color: '#45161C',
        colorName: 'Brown'
    },
    {
        color: '#CD7F32',
        colorName: 'Bronze'
    },
];


class Box extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentColor: "#000"
        }
    }

    setColor(color) {
        this.setState(state => ({
            currentColor: color
        }));
    }

    render() {
        return <div id="square" style={{backgroundColor: this.state.currentColor}}>{items.map (item => <button key={item.color} className="btn"
            onClick={this.setColor.bind(this, item.color)}>{item.colorName}</button>)}</div>;
    }
}


export default Box;
